package main

import "testing"

func TestGetHostnameValid(t *testing.T) {
	hosts := []struct {
		given    string
		expected string
	}{
		{"www.google.com", "www.google.com"},
		{"www.google.com:8080", "www.google.com"},
		{"[::1]", "::1"},
		{"[::1]:8080", "::1"},
		{"127.0.0.1:8080", "127.0.0.1"},
	}

	for _, host := range hosts {
		recv, err := getHostname(host.given)
		if err != nil || recv != host.expected {
			t.Fail()
		}
	}
}
