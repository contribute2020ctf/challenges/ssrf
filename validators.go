package main

import (
	"context"
	"errors"
	"net"
	"net/http"
	"net/url"
	"strings"
)

var errInvalidURL = errors.New("invalid url format")
var errInvalidScheme = errors.New("http(s) must be used")
var errRestrictedHost = errors.New("restricted address")

// hostname that could be used for rebinding 7f000001.acd9c068.rbndr.us

// url validation function: it will run different validations based on difficulty level
// firstValidator is always run
// secondValidator() on levels 2 & 3
func validateURL(ctx context.Context, urlStr string) error {
	// Parse urlStr into url struct, return error if not a valid uri
	u, err := url.ParseRequestURI(urlStr)
	if err != nil {
		return errInvalidURL
	}

	// get associated IPAddrs
	ipAddrs, err := getIPAddrs(ctx, u.Host)
	if err != nil {
		return err
	}

	url := SearchURL{
		URL:     u,
		IPAddrs: ipAddrs,
	}

	err = url.firstValidator(ctx)
	if err != nil {
		return err
	}

	if level > 1 {
		err = url.secondValidator(ctx)
		if err != nil {
			return err
		}
	}

	return nil
}

// validate only that url uses http(s)
func (url SearchURL) firstValidator(ctx context.Context) error {
	if url.URL.Scheme != "https" && url.URL.Scheme != "http" {
		return errInvalidScheme
	}

	for _, ip := range url.IPAddrs {
		if ip.IsLinkLocalUnicast() || isPrivateUnicast(ip) {
			return errRestrictedHost
		}
	}
	return nil
}

// validate that url does not directly reference a localhost address
func (url SearchURL) secondValidator(ctx context.Context) error {
	for _, ip := range url.IPAddrs {
		if !ip.IsGlobalUnicast() {
			return errRestrictedHost
		}
	}

	return nil
}

// validate destination of any redirects
func redirectValidator(r *http.Request, via []*http.Request) error {
	ctx := r.Context()

	return validateURL(ctx, r.URL.String())
}

func getIPAddrs(ctx context.Context, host string) ([]net.IP, error) {
	hostname, err := getHostname(host)
	if err != nil {
		return nil, err
	}

	// Check if it is an IP address, otherwise do DNS lookup
	if i := net.ParseIP(hostname); i != nil {
		return []net.IP{i}, nil
	}

	ipStrs, err := dnsResolver.LookupHost(ctx, hostname)
	if err != nil {
		return nil, err
	}
	ipAddrs := make([]net.IP, len(ipStrs))
	for i, ipStr := range ipStrs {
		ipAddrs[i] = net.ParseIP(ipStr)
	}

	return ipAddrs, nil
}

func getHostname(host string) (string, error) {
	i := strings.LastIndex(host, ":")
	if i < 0 {
		return host, nil
	}

	if i > 0 && host[0] == '[' && host[i-1] != ']' {
		//IPv6 address in [] without port, must remove [] for raw IP address
		return strings.Trim(host, "[]"), nil
	}

	h, _, err := net.SplitHostPort(host)
	if err != nil {
		return "", err
	}

	return h, nil
}

func isPrivateUnicast(ip net.IP) bool {
	isPrivate := false
	for _, n := range []string{
		"10.0.0.0/8",
		"172.16.0.0/12",
		"192.168.0.0/16",
		"fc00::/7",
	} {
		_, netwrk, _ := net.ParseCIDR(n)
		isPrivate = isPrivate || netwrk.Contains(ip)
	}
	return isPrivate
}
