FROM golang:1.13.5-alpine3.10 as builder

ENV PROJECT gitlab.com/contribute2020ctf/challenges/ssrf
WORKDIR /go/src/$PROJECT

COPY . .
RUN go build

FROM alpine:3.10 as release
RUN apk add --no-cache ca-certificates \
    busybox-extras net-tools bind-tools
ENV PROJECT gitlab.com/contribute2020ctf/challenges/ssrf
WORKDIR /app
COPY --from=builder /go/src/$PROJECT/ssrf ./ssrf
COPY ./templates ./templates
EXPOSE 8080
ENTRYPOINT [ "/app/ssrf" ]
