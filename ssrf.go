package main

import (
	"context"
	"errors"
	"html/template"
	"net"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

const (
	port = "8080"
)

var (
	level       = 1
	flag        = "SSRFWINNINGFLAG"
	resolverIP  = "8.8.8.8"
	dnsResolver net.Resolver

	printHeaders = []string{
		"Server",
		"Content-Security-Policy",
		"Feature-Policy",
		"Referrer-Policy",
		"Strict-Transport-Security",
		"X-Frame-Options",
		"X-Content-Type-Options",
		"X-XSS-Protection",
	}
)

func init() {
	if l := os.Getenv("LEVEL"); l == "2" || l == "3" {
		level, _ = strconv.Atoi(l)
	}

	if os.Getenv("FLAG") != "" {
		flag = os.Getenv("FLAG")
	}

	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

	dnsResolver = net.Resolver{
		PreferGo: true,
		Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
			d := net.Dialer{}
			return d.DialContext(ctx, "udp", net.JoinHostPort(resolverIP, "53"))
		},
	}

}

//SearchURL allows the addition of methods on the type
type SearchURL struct {
	URL     *url.URL
	IPAddrs []net.IP
}
type ctxKeyLog struct{}

// Create templates cache to avoid parsing on each request
var templates = template.Must(template.New("").
	Funcs(template.FuncMap{
		"join": strings.Join,
	}).ParseGlob("templates/*.html"))

// Determine if the connection is a winner, based on source ip and method
func isWinner(r *http.Request) (bool, error) {
	host, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return false, err
	}

	return r.Method == http.MethodHead &&
		(host == "127.0.0.1" || host == "::1"), nil
}

// makes http HEAD request to get headers from destination
func getHeaders(ctx context.Context, urlStr string) (http.Header, error) {
	transport := &http.Transport{
		DialContext: (&net.Dialer{
			Resolver: &dnsResolver,
			Timeout:  2 * time.Second,
		}).DialContext,
		TLSHandshakeTimeout: 2 * time.Second,
	}
	client := &http.Client{
		Transport: transport,
		Timeout:   5 * time.Second,
	}

	// run redirectValidator() on level 3 difficulty
	if level > 2 {
		client.CheckRedirect = redirectValidator
	}

	req, _ := http.NewRequestWithContext(ctx, http.MethodHead, urlStr, nil)
	req.Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:71.0) Gecko/20100101 Firefox/71.0")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return resp.Header, nil
}

func getHeadersToPrint(headers http.Header) http.Header {
	ret := http.Header{}

	for _, k := range printHeaders {
		v := headers.Get(k)
		if v == "" {
			v = "Not Set"
		}
		ret.Add(k, v)
	}

	if v := headers.Get("X-CTF-FLAG"); v != "" {
		ret.Add("X-CTF-FLAG", v)
	}

	return ret
}

func renderErrorPage(ctx context.Context, w http.ResponseWriter, err error, url string) {
	logger := ctx.Value(ctxKeyLog{}).(log.FieldLogger)
	var code int

	if errors.Is(err, errInvalidURL) || errors.Is(err, errInvalidScheme) || errors.Is(err, errRestrictedHost) {
		code = http.StatusBadRequest
	} else {
		code = http.StatusInternalServerError
	}

	logger.WithFields(log.Fields{
		"url":   url,
		"error": err,
		"code":  code}).Error("request error")

	w.WriteHeader(code)
	renderPage(ctx, w, "results", map[string]interface{}{
		"url":   url,
		"error": err.Error(),
	})
}

// page renderer
func renderPage(ctx context.Context, w http.ResponseWriter, tmpl string, d map[string]interface{}) {
	logger := ctx.Value(ctxKeyLog{}).(log.FieldLogger)

	err := templates.ExecuteTemplate(w, tmpl, d)
	if err != nil {
		logger.WithField("error", err).Error("failed to render page")
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// single handler function that handles / path
func pageHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	requestID, _ := uuid.NewRandom()

	logger := log.WithFields(log.Fields{
		"ip":        r.RemoteAddr,
		"requestID": requestID,
		"path":      r.URL.Path,
		"queryURL":  r.FormValue("url"),
	})
	logger.Debug("new request")

	ctx = context.WithValue(ctx, ctxKeyLog{}, logger)

	// Check if the connection signifies a winner
	// If so, set flag and render index since body is ignored
	// If not, handle request normally

	if winner, err := isWinner(r); err != nil {
		renderErrorPage(ctx, w, err, "")
		return
	} else if winner {
		w.Header().Set("X-CTF-FLAG", flag)
		renderPage(ctx, w, "index", map[string]interface{}{
			"url": "",
		})
		return
	}

	urlStr := r.FormValue("url")

	// If url is not set, render index
	if urlStr == "" {
		renderPage(ctx, w, "index", map[string]interface{}{
			"url": "",
		})
		return
	}

	// Validate the URL based on given level, render error if not valid
	if err := validateURL(ctx, urlStr); err != nil {
		renderErrorPage(ctx, w, err, urlStr)
		return
	}

	// Make head request and render results, render error if error
	headers, err := getHeaders(ctx, urlStr)
	if err != nil {
		renderErrorPage(ctx, w, err, urlStr)
		return
	}

	msg := ""
	if headers.Get("X-Ctf-Flag") != "" {
		logger.Info("we have a winner")
		msg = "Congratulations! We have a winner."
	}

	renderPage(ctx, w, "results", map[string]interface{}{
		"url":     urlStr,
		"headers": getHeadersToPrint(headers),
		"msg":     msg,
	})
}

func faqHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	requestID, _ := uuid.NewRandom()

	logger := log.WithFields(log.Fields{
		"ip":        r.RemoteAddr,
		"requestID": requestID,
	})
	logger.Debug("new faq request")

	ctx = context.WithValue(ctx, ctxKeyLog{}, logger)

	renderPage(ctx, w, "faq", map[string]interface{}{
		"level": level,
	})
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", pageHandler).Methods(http.MethodGet, http.MethodHead)
	r.HandleFunc("/faq", faqHandler).Methods(http.MethodGet, http.MethodHead)
	log.Fatal(http.ListenAndServe(":"+port, r))
}
